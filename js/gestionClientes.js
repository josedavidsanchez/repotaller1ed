var clientesObtenidos;
function getClientes() {
  var url = "https://services.odata.org/V4/Northwind/Northwind.svc/Customers";
  var request = new XMLHttpRequest();
  request.onreadystatechange = function () {
    if (this.readyState == 4 && this.status == 200) {
      //console.table(JSON.parse(request.responseText).value);
      clientesObtenidos = request.responseText;
      procesarClientes();
    }
  }
  request.open("GET",url, true);
  request.send();
}

function procesarClientes() {
    var JSONProductos =JSON.parse(clientesObtenidos);
    //alert(JSONProductos.value[0]);
    var divTabla = document.getElementById("divTablaClientes");
    var tabla = document.createElement("table");
    var tbody = document.createElement("tbody");

    tabla.classList.add("table");
    tabla.classList.add("table-striped");
    //https://www.countries-ofthe-world.com/flags-normal/flag-of-""
    var rutaBandera = "https://www.countries-ofthe-world.com/flags-normal/flag-of-"
    for (var i = 0; i < JSONProductos.value.length; i++) {
      //console.log(JSONProductos.value[i].ProductName);
      var nuevaFila = document.createElement("tr");

      var columnaNombre = document.createElement("td");
      columnaNombre.innerText = JSONProductos.value[i].ContactName;

      var columnaCiudad = document.createElement("td");
      columnaCiudad.innerText = JSONProductos.value[i].City;

      var columnaBandera = document.createElement("td");
      var imgBandera = document.createElement("img");
      if (JSONProductos.value[i].Country == 'UK'){
          imgBandera.src =  rutaBandera + "United-Kingdom.png";
      }else {
          imgBandera.src =  rutaBandera + JSONProductos.value[i].Country + ".png";
      }

      //imgBandera.src =  rutaBandera + JSONProductos.value[i].Country + ".png";
      imgBandera.style.height = "40px"
      imgBandera.style.width = "70px"
      columnaBandera.appendChild(imgBandera)

      //columnaPais.innerText = JSONProductos.value[i].Country;

      nuevaFila.appendChild(columnaNombre);
      nuevaFila.appendChild(columnaCiudad);
      nuevaFila.appendChild(columnaBandera);

      tbody.appendChild(nuevaFila);

    }

    tabla.appendChild(tbody);
    divTabla.appendChild(tabla);
}
